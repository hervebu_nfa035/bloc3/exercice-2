package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Etends JPanel afin de définir  paintComponent pour afficher l'image du badge
 */
public class BadgePanel extends JPanel {

    private static final String RESOURCES_PATH = "badges-gui/src/main/resources/";

    public DirectAccessBadgeWalletDAO dao;
    public DigitalBadge badge;

    /**
     * Constructeur permettant de définir dao et badge
     * @param dao
     * @param badge
     */
    public BadgePanel (DirectAccessBadgeWalletDAO dao, DigitalBadge badge) {
        this.dao = dao;
        this.badge = badge;

    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            dao.getBadgeFromMetadata(bos, badge);
            g.drawImage(ImageIO.read(new ByteArrayInputStream(bos.toByteArray())), 0, 0, this.getWidth(), this.getHeight(), this);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }


}
